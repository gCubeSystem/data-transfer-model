# Changelog for org.gcube.data.transfer.data-transfer-model

## Unreleased [v1.2.5] 2020-07-16

### Fixes

- Integration with gcube distribution (boms 2.0.0)
